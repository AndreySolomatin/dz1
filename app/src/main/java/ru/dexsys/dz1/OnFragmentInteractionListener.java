package ru.dexsys.dz1;

/**
 * Created by Andrey Solomatin on 04-Jun-18.
 */
public interface OnFragmentInteractionListener {

    void onFragmentInteraction(int count);

}
